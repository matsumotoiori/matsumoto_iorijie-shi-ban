<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>簡易Twitter</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor=white>
	<c:if test="${ empty loginUser }">
		<a href="login">ログイン</a>
		<a href="signup">登録する</a>
	</c:if>
	<c:if test="${ not empty loginUser }">
		<a href="management">ユーザー管理</a>
		<a href="logout">ログアウト</a>
		<a href="contribution">新規投稿</a>
		<br />
		<font size="20">掲示板</font>
		<h2>
			<font size="5"><c:out value="${loginUser.name}" /></font>
		</h2>
	</c:if>
	<br />
	<c:forEach items="${messages}" var="message">
		<form action="deleteContribution" method="post">
			────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
			<font size="2">NAME:</font> <b><span class="name"><c:out
						value="${message.name}" /></span></b> &ensp;
			<fmt:formatDate value="${message.created_date}"
				pattern="yyyy/MM/dd HH:mm:ss" />
			&ensp; <font size="2">ログインID:</font> <font size="4"><span
				class="loginID"><c:out value="${message.loginID}" /></span></font> &ensp;
			<c:if test="${loginUser.name == message.name}">
			<button type="submit" name="delete">削除</button>
			</c:if>
			<br /> <font size="2">件名:</font>
			<c:out value="${message.title}" />
			&ensp; <font size="2">カテゴリ:</font>,
			<c:out value="${message.category}" />
			<br /> <font size="5">本文:</font> <font size="5"><c:out
					value="${message.text}" /></font> <input type="hidden" name="id"
				value="${message.id}">
		</form>
		<br />
		<br />
		<form action="comment" method="post">
			<textarea name="comment" cols="100" rows="5" class="tweet-box"></textarea>
			<br /> <input type="submit" value="コメント">（140文字まで） <br /> <input
				type="hidden" name="id" value="${message.id}">
		</form>

		<c:forEach items="${comments}" var="comment">
		<form action="CommentDelete" method="post">
			<c:if test="${message.id == comment.comment_id}">
				<font size="2">NAME:<c:out value="${comment.name}" /> &ensp;
					<fmt:formatDate value="${comment.created_date}"
						pattern="yyyy/MM/dd HH:mm:ss" /></font>
				&ensp;
				<br />
				<font size="2">コメント: <c:out value="${comment.text}" /></font>
				<button type="submit" name="delete">削除</button>
				<input
				type="hidden" name="id" value="${comment.id}">
				<br />
			</c:if>
			<input
				type="hidden" name="id" value="${comment.comment_id}">
			</form>
		</c:forEach>

	</c:forEach>
	<div class="copylight">Copyright(c)YourName</div>
</body>
</html>