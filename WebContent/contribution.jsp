<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
     	<div class="main-contents">
			<div class="header">
					<a href="./">ホーム</a>
    		</div>
			<div class="form-area">
					<form action="contribution" method="post">
					件名<br />
					<textarea name="title" cols="30" rows="1" class="tweet-box"></textarea><br />
						カテゴリ<br />
					<textarea name="category" cols="30" rows="1" class="tweet-box"></textarea><br />
						いま、どうしてる？ <br />
						<textarea name="messages" cols="100" rows="5" class="tweet-box"></textarea><br />
						<input type="submit" value="つぶやく">（140文字まで）
					</form>
			<div class="copylight"> Copyright(c)YourName</div>
 	        </div>
 	    </div>
	</body>
</html>