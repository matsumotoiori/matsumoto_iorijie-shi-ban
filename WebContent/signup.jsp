<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">
                <br />

                 <label for="name">名前&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;</label> <input name="name" id="name" />（名前はあなたの公開プロフィールに表示されます）
                 <br /><br />

                     <label for="loginID">ログインID&emsp;&emsp;&emsp;</label> <input name="loginID"
                    id="loginID" />
                    <br /> <br />

                    <label for="password">パスワード&emsp;&emsp;&emsp;</label> <input
                    name="password" type="password" id="password" />
                    <br /><br />

<!--                     <label for="password">パスワード確認用</label> <input name="password" id="password" />
                    <br /><br /> -->

                    <label for="branch">支店名</label>
                    <select name="branch" class="form-control">
				<c:forEach var="branch" items="${branch}">
					<option value="<c:out value="${branch.key}" />" <c:if test="${editUser.branch == branch.key}">selected</c:if>><c:out value="${branch.value}" /></option>
				</c:forEach>
				</select>
                    <br /><br />

                    <label for="division">部署・役職</label>
                    <select name="division" class="form-control">
				<c:forEach var="division" items="${division}">
					<option value="<c:out value="${division.key}" />" <c:if test="${editUser.division == division.key}">selected</c:if>><c:out value="${division.value}" /></option>
				</c:forEach>
				</select>
                    <br /><br />

                    <input type="submit" value="登録" />
                     <br />

                      <a href="./">戻る</a>
            </form>
            <div class="copyright">Copyright(c)Your Name</div>
        </div>
    </body>
</html>