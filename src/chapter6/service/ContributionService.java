package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter6.beans.Contribution;
import chapter6.beans.UserContribution;
import chapter6.dao.ContributionDao;
import chapter6.dao.UserContributionDao;

public class ContributionService {

    public void register(Contribution message) {

        Connection connection = null;
        try {
            connection = getConnection();

            ContributionDao contributionDao = new ContributionDao();
            contributionDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void delete(Contribution delete) {

        Connection connection = null;
        try {
            connection = getConnection();

            ContributionDao contributionDao = new ContributionDao();
            contributionDao.delete(connection, delete);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


    private static final int LIMIT_NUM = 1000;

    public List<UserContribution> getContribution() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserContributionDao contributionDao = new UserContributionDao();
    		List<UserContribution> ret = contributionDao.getUserContributions(connection, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }
}