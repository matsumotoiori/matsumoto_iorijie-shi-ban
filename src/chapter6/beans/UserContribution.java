package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class UserContribution implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String loginID;
    private String name;
    private int user_id;
    private String text;
    private String title;
    private String category;
    private Date created_date;
    private int postdelete;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginID() {
		return loginID;
	}
	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getUserId() {
		return user_id;
	}
	public void setUserId(int user_id) {
		this.user_id = user_id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public int getPostdelete() {
		return postdelete;
	}
	public void setPostdelete(int postdelete) {
		this.postdelete = postdelete;
	}

}