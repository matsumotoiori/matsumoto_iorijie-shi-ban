package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserContribution;
import chapter6.exception.SQLRuntimeException;

public class UserContributionDao {

    public List<UserContribution> getUserContributions(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.text as text, ");
            sql.append("messages.title as title, ");
            sql.append("messages.category as category, ");
            sql.append("messages.user_id as user_id, ");
            sql.append("user.loginID as loginID, ");
            sql.append("user.name as name, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN user ");
            sql.append("ON messages.user_id = user.id ");
            sql.append("WHERE ");
			sql.append("postdelete = 0 ");
            sql.append("ORDER BY created_date DESC limit " + num);


            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserContribution> ret = toUserContributionList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserContribution> toUserContributionList(ResultSet rs)
            throws SQLException {

        List<UserContribution> ret = new ArrayList<UserContribution>();
        try {
            while (rs.next()) {
                String loginID = rs.getString("loginID");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String text = rs.getString("text");
                String title = rs.getString("title");
                String category = rs.getString("category");
                Timestamp createdDate = rs.getTimestamp("created_date");


                UserContribution contribution = new UserContribution();
                contribution.setLoginID(loginID);
                contribution.setName(name);
                contribution.setId(id);
                contribution.setUserId(userId);
                contribution.setText(text);
                contribution.setTitle(title);
                contribution.setCategory(category);
                contribution.setCreated_date(createdDate);

                ret.add(contribution);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}