package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter6.beans.Contribution;
import chapter6.exception.SQLRuntimeException;

public class ContributionDao {

    public void insert(Connection connection, Contribution message ) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO messages ( ");
            sql.append("user_id");
            sql.append(", text");
            sql.append(", title");
            sql.append(", category");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(", postdelete");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); // text
            sql.append(", ?"); // title
            sql.append(", ?"); // category
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(", 0"); // postdelete
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUserId());
            ps.setString(2, message.getText());
            ps.setString(3, message.getTitle());
            ps.setString(4, message.getCategory());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void delete(Connection connection, Contribution delete) {

    	 PreparedStatement ps = null;
         try {
        	 String sql = "UPDATE messages SET postdelete = 1 WHERE id = ?";
        	 ps = connection.prepareStatement(sql);
             ps.setInt(1,delete.getId());

 			 ps.executeUpdate();
         } catch (SQLException e) {
             throw new SQLRuntimeException(e);
         } finally {
             close(ps);
         }
    }
}