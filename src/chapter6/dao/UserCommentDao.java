package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserComment;
import chapter6.exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComments(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comment.id as id, ");
            sql.append("comment.text as text, ");
            sql.append("comment.comment_id as comment_id, ");
            sql.append("comment.user_id as user_id, ");
            sql.append("user.loginID as loginID, ");
            sql.append("user.name as name, ");
            sql.append("comment.created_date as created_date ");
            sql.append("FROM comment ");
            sql.append("INNER JOIN user ");
            sql.append("ON comment.user_id = user.id ");
            sql.append("INNER JOIN messages ");
            sql.append("ON comment.comment_id = messages.id ");
            sql.append("WHERE ");
			sql.append("commentdelete = 0 ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
                String loginID = rs.getString("loginID");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int user_id = rs.getInt("user_id");
                String text = rs.getString("text");
                int comment_id=rs.getInt("comment_id");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserComment Comment = new UserComment();
                Comment.setLoginID(loginID);
                Comment.setName(name);
                Comment.setId(id);
                Comment.setUserId(user_id);
                Comment.setText(text);
                Comment.setComment_id(comment_id);
                Comment.setCreated_date(createdDate);

                ret.add(Comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}