package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter6.beans.Comment;
import chapter6.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment ) {//Connectionデータベースと接続

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comment ( ");
            sql.append("user_id");
            sql.append(", text");
            sql.append(", comment_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(", commentdelete");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); // text
            sql.append(", ?"); // comment_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(", 0");
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());//sql文をデータベースに送る

            ps.setInt(1, comment.getUserId());
            ps.setString(2, comment.getText());
            ps.setInt(3, comment.getComment_id());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void delete(Connection connection, Comment delete) {

   	 PreparedStatement ps = null;
        try {
       	 String sql = "UPDATE comment SET commentdelete = 1 WHERE id = ?";
       	 ps = connection.prepareStatement(sql);
            ps.setInt(1,delete.getId());

			 ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
   }
}