package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.ManagementUser;
import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO user ( ");
            sql.append("loginID");
            sql.append(", name");
            sql.append(", password");
            sql.append(", branch");
            sql.append(", division");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // loginID
            sql.append(", ?"); // name
            sql.append(", ?"); // password
            sql.append(", ?"); // branch
            sql.append(", ?"); // division
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginID());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getBranch());
            ps.setInt(5, user.getDivision());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User getUser(Connection connection, String loginID,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM user WHERE loginID = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, loginID);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginID = rs.getString("loginID");
                String name = rs.getString("name");
                String password = rs.getString("password");
                int branch = rs.getInt("branch");
                int division = rs.getInt("division");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");


                User user = new User();
                user.setId(id);
                user.setLoginID(loginID);
                user.setName(name);
                user.setPassword(password);
                user.setBranch(branch);
                user.setDivision(division);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public User getUser(Connection connection, int id) {

    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM user WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, id);

    		ResultSet rs = ps.executeQuery();
    		List<User> userList = toUserList(rs);
    		if (userList.isEmpty() == true) {
    			return null;
    		} else if (2 <= userList.size()) {
    			throw new IllegalStateException("2 <= userList.size()");
    		} else {
    			return userList.get(0);
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

    public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  loginID = ?");
			sql.append(", name = ?");
			sql.append(", password = ?");
			sql.append(", branch = ?");
			sql.append(", division = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginID());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getDivision());
			ps.setInt(6, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

    public List<ManagementUser> getAllManagementUser(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "";
			sql += "SELECT";
			sql += "   u.*";
			sql += " , b.branch AS branch_name";
			sql += " , d.division AS division_name";
			sql += " FROM";
			sql += "   user AS u";
			sql += " LEFT JOIN";
			sql += "   branches AS b";
			sql += " ON";
			sql += "   u.branch = b.id";
			sql += " LEFT JOIN";
			sql += "   divisiones AS d";
			sql += " ON";
			sql += "   u.division = d.id";

			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<ManagementUser> managementList = toManagementUserList(rs);
			if (managementList.isEmpty()) {
				return null;
			} else {
				return managementList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

    private List<User> toManagementList(ResultSet rs) throws SQLException {
		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setLoginID(rs.getString("loginID"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranch(rs.getInt("branch"));
				user.setDivision(rs.getInt("division"));
				user.setCreatedDate(rs.getTimestamp("created_at"));
				user.setUpdatedDate(rs.getTimestamp("updated_at"));
				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	private List<ManagementUser> toManagementUserList(ResultSet rs) throws SQLException {
		List<ManagementUser> ret = new ArrayList<ManagementUser>();
		try {
			while (rs.next()) {
				ManagementUser management = new ManagementUser();
				management.setId(rs.getInt("id"));
				management.setLoginId(rs.getString("loginID"));
				management.setPassword(rs.getString("password"));
				management.setName(rs.getString("name"));
				management.setBranch(rs.getInt("branch"));
				management.setDivision(rs.getInt("division"));
				management.setBranchName(rs.getString("branch_name"));
				management.setDivisionName(rs.getString("division_name"));
				management.setCreatedDate(rs.getDate("created_date"));
				management.setUpdatedDate(rs.getDate("updated_date"));
				ret.add(management);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<User> getAllUser(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM user";
			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<User> managementList = toManagementList(rs);
			if (managementList.isEmpty()) {
				return null;
			} else {
				return managementList;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public boolean duplicateCheck(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT id FROM users WHERE login_id = ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1, user.getLoginID());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return false;
			}
			return true;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public boolean idDuplicateCheck(Connection connection, User user) {
		PreparedStatement ps = null;
		try {
			String sql = "";
			sql += "SELECT";
			sql += "  COUNT(*) as count";
			sql += " FROM";
			sql += "  users AS u";
			sql += " WHERE";
			sql += "  NOT EXISTS";
			sql += " (";
			sql += " SELECT";
			sql += "  u.login_id";
			sql += " FROM";
			sql += "  users";
			sql += " WHERE";
			sql += "  users.id = ?";
			sql += " AND";
			sql += "  users.login_id = ?";
			sql += " )";
			sql += " AND";
			sql += "  u.login_id = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, user.getId());
			ps.setString(2, user.getLoginID());
			ps.setString(3, user.getLoginID());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt("count") == 0) return true;
			}
			return false;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public boolean otherGeneralExists(Connection connection) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT COUNT(*) AS count FROM users WHERE division = ?";
			ps = connection.prepareStatement(sql);
			ps.setInt(1, 1);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				if (rs.getInt("count") <= 1) return false;
			}
			return true;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
